#!/bin/bash

deving(){
	minikube kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4
	minikube kubectl expose deployment hello-minikube --type=NodePort --port=8080

	minikube service hello-minikube

}

# V ------------------ Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'
DateTime=$(date)
# -------------------------- Setup -------------------

RunCheck(){
# Validates if this is a fresh install or not.
# Looks for dmt0.txt and creates a shortcut in ~/.bash_aliases if it does not exists.
if ![ -f "~/micro8s-menu/dmt0.txt" ] 
	then
	echo -e "KubeMenu=~/micro8s-menu/KubeMenu.sh" >> ~/.bash_aliases
# Execute CreateMarker
	CreateMarker
	else
	lastmessage="Welcome back $USER"
	fi
}

CreateMarker(){
# Creates a marker file in the DMT folder. 
if [ -f !"~/micro8s-menu/dmt0.txt" ]
	then
#Create the marker (dmt0.txt) file.	
	touch ~/micro8s-menu/dmt0.txt
# Copy install time to the marker.	
	echo "Installed and run on $DateTime" >> ~/micro8s-menu/dmt0.txt

	lastmessage="Created a shortcut in .bash_aliases"
	else 
	lastmessage="Proceed $USER"
	fi
}

# ------- Git Managment -------

GitBranch(){
	cd ~/micro8s-menu/
	BranchName=$(git branch)
	cd ..
	echo "Working on the Git Branch: $BranchName "
}

KMupdate(){
	cd ~/micro8s-menu/
	echo "Checking for an update"
    git pull
    cd ..
}

KMcommit(){
	cd ~/micro8s-menu/
	git config user.email "an@example.com"
	git config user.name "AddCommitPush"

	read -rp "Enter your commit message: " GitMessage
    git add . && git commit -m " $GitMessage "
    git push git@gitlab.com:engy4life~/micro8s-menu.git
    cd ..
}

# --------------- Minikube Options --------------

# Source: https://minikube.sigs.k8s.io/docs/start/
InstallMiniKube(){
# Install Minikube on the host machine.
	curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
	sudo install minikube-linux-amd64 /usr/local/bin/minikube
	lastmessage="Ran MiniKube Installer."
}

RunMiniKubeDashboard(){
# Start a new MiniKube.
	minikube start
# View container status.
	kubectl get po -A
# Open Dashboard in a browser and run in background.	
	minikube dashboard #&& disown -h
	lastmessage="Open Dashboard in default browser."
}

DeployDemo(){
# Create a sample deployment and expose it on port 8080:
	kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4
	kubectl expose deployment hello-minikube --type=NodePort --port=8080

	microkube kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4
	microkube kubectl expose deployment hello-minikube --type=NodePort --port=8080

#	It may take a moment, but your deployment will soon show up when you run:
#	kubectl get services hello-minikube
# The easiest way to access this service is to let minikube launch a web browser for you:
	minikube service hello-minikube
#	Alternatively, use kubectl to forward the port:
#	kubectl port-forward service/hello-minikube 7080:8080
#	Your application is now available at http://localhost:7080/
}




# 0 ----------------------- Main Menu -----------------------

MainMenu(){
clear
MenuTitle=" - Kubernetes Management Tools - "
Description=" Setup and manage a variety of Kubernetes. "

# Display menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
GitBranch 
	echo "-----------------------------------"
	echo "0.  Update Kubernetes Management Tools "
	echo "10. Kuberenetes Menu. "
	echo "20. micro8s Menu. "
	echo "31. Install MiniKube upon the Host."
	echo "32. Run MiniKube dashboard in the background"
	echo "33. deploy and view the Hello World demo."
	echo "80. Developing "
	echo "Q.  Quit			Exit this Menu"
#   echo "ACP                      Shortcut to git Add, Commit & Push"
	echo " "
	echo " $lastmessage " 
	echo " " 
}

read_options(){
	local choice
	read -p "Enter the desired item number: " choice
	case $choice in
    0)  clear && KMupdate ;;
	10) clear && ~/micro8s-menu/KubeRunner.sh ;;
	20) clear && ~/micro8s-menu/k8runner.sh ;;
	31) InstallMiniKube ;;
	32)	RunMiniKubeDashboard ;;
	33) DeployDemo ;;
	80) deving ;;
	ACP) KMcommit ;;
    q|Q) clear && echo " See you later $USER! " && exit 0;;
*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

#Start the Main Menu
RunCheck
MainMenu


