#!/bin/bash


# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

clear

pause(){
# waits for user to hit the enter key...	
  read -p "Press [Enter] key to continue..." fackEnterKey
}



# 1 ------------------------------ Setup -----------------------------

SystemUUAA(){
# prepare host 	
	sudo apt update
    sudo apt upgrade -y
    sudo apt autoremove -y
    sudo apt autoclean
# 	
	SAI="sudo apt install -y" 
	$SAI snapd
	$SAI bash-completion
}

SSHkeyGenCopy(){
# will verify if a key exists before proceedings 
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
	HostSSHKey=$(cat $HOME/.ssh/id_rsa.pub)
	echo $HostSSHKey
	lastmessage="Your SSH Key has been created."
}

InstallKubernetes(){
# Use apt to the start interactive installer 
	$SAI kubernetes
	kubernetes install
}

ConfigureMicro8s(){
# Add the current user to the Kubernetes group
	sudo usermod - a -G microk8s $USER
	sudo chown -f -R $USER ~/.kube
# logout to apply changes
	echo "logout or restart to complete group addition"
}

SetupBashCompletion(){
# enable BASH completion 
	echo "source <(kubectl completion bash)" >> ~/.bashrc
	echo "source <(kubeadm completion bash)" >> ~/.bashrc

# Reload bash without logging out
	source ~/.bashrc 
}


# -------------------------- Usage ---------------------------

StartMicroK8s(){
	microk8s.start
	lastmessage="MicroK8s started"
}

StopMicroK8s(){
	microk8s.stop
	lastmessage="MicroK8s stopped"
}

RestartMicroK8s(){
	microk8s stop && microk8s start
	lastmessage="MicroK8s restarted"
}

ListStatus(){
# Display Addons
	microk8s status
}

AddOnEnable(){
	read -rp "Choose the desired Addons to enable: " EnableAddon
	microk8s enable $EnableAddon
}

AddOnDisable(){
	read -rp "Choose the desired Addons to remove: " DisableAddon
	microk8s disable $DisableAddon
}

DisplaySecretToken(){
# Display Secret Token
	token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
	microk8s kubectl -n kube-system describe secret $token
}

NewMicrobot(){
# Create and expose a microbot service
	microk8s kubectl create deployment microbot --image=dontrebootme/microbot:v1
	microk8s kubectl scale deployment microbot --replicas=2
	microk8s kubectl expose deployment microbot --type=NodePort --port=80 --name=microbot-service 
}

# -------------------------- Main Menu -----------------------

Microk8smenu(){

Microk8s_menus(){

MenuTitle=" Microk8s Menu"
MenuDescription=" Perform local Kubernetes operations. "

	echo " "	
	echo " $MenuTitle " 
	echo " $MenuDescription " 
	echo " -------------------------------- "
	echo "1.  Update, Upgrade, Autoremove and Autoclean "
	echo "2.  Interactive Kubernetes installer"
	echo "3.  Configure Micro8s"
	echo "4.  Add kubernetes BASH completion."
	echo "10.  "
	echo "20.  "
	echo "21.  Microk8s addon status"
	echo "22.  Enable Addons "
	echo "23.  Disable Addons "
	echo "24.  Display Config"
	echo "25.  Restart MicroK8s"
	echo "26.  Start MicroK8s"
	echo "27.  Stop MicroK8s"
	echo "30.  Display all pods "
	echo "31.  Display all nodes"
	echo "32.  Display all services "
	echo "40.  Generate an SSH Key and copy to the HostSSHKey variable"
	echo "41.  "
	echo "42.   "
	echo "50.  Test service"
	echo "70.   "
#    echo "80.  Logout $USER from this Server "
	echo "81.   "
	echo "90.   "
	echo "99. Return to Main Menu "
	echo
	echo "$lastmessage "
}

Microk8s_options(){

local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) SystemUUAA ;;
		2) InstallKubernetes ;;
		3) ConfigureMicro8s ;;
		4) SetupBashCompletion ;;
		10) SetupMiniKube ;;
		20)  ;;
		21) ListStatus ;;
		22) AddOnEnable ;;
		23) AddOnDisable ;;
		24) Microk8s config ;;
		25) RestartMicroK8s ;;
		26) StartMicroK8s ;;
		27) StopMicroK8s ;;
		30) microk8s kubectl get all --all-namespaces ;;
		31) microk8s kubectl get nodes ;;
		32) microk8s kubectl get services ;;
		40) SSHkeyGenCopy ;;
		41)  ;;
        42)  ;;
		50)  NewMicrobot ;;
		60)  ;;
		70)  ;;
		80) LogOffUser ;;
		81)  ;;
		99) clear &&  exit ;;
		*) echo -e "${RED} You can't do that..... ${STD}" && sleep 2
	esac

}
    while true
    do
	Microk8s_menus
	Microk8s_options
    done
}


 RunCheck
 Microk8smenu
# SSHkeycopy
